import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DU {

    public static void main(String[] args) {

        if(args.length == 0) {
            throw new IllegalArgumentException("Please provide directory path to analyze");
        }

        Path rootPath = Paths.get(args[0]);
        DirAnalyzer analyzer = new DirAnalyzer(rootPath);
        analyzer.displayFolderContent();
    }

}

class DirAnalyzer {

    Path rootDir;

    public DirAnalyzer(Path rootDir) {
        this.rootDir = rootDir;
    }

    public void displayFolderContent() {
        if(Files.notExists(rootDir)) {
            throw new IllegalArgumentException("Directory does not exists");
        }
        try {
            Files.list(rootDir)
                    .map(name -> DirItemFactory.fromPath(name))
                    .sorted()
                    .forEach(System.out::println);
        } catch (IOException e) {
            throw new IllegalStateException("Something went wrong: " + e.getMessage());
        }
    }

}

class DirItemFactory {

    public static DirItem fromPath(Path path) {
        DirItem dirItem;
        if(Files.isDirectory(path)) {
            dirItem = new DirItem(path, Type.DIR);
        } else {
            dirItem = new DirItem(path, Type.FILE);
        }
        dirItem.calculateDirItemSize();
        return dirItem;
    }

}

class DirItem implements Comparable<DirItem> {

    private static final long KILOBYTE = 1024;

    Type type;
    Path path;
    Long size;

    public DirItem(Path path, Type type) {
        this.path = path;
        this.type = type;
    }

    public void calculateDirItemSize() {
        try {
            this.size = calculateFileOrFolderSize(path);
        } catch (IOException e) {
            throw new IllegalStateException("Something went wrong: " + e.getMessage());
        }
    }

    private long calculateFileOrFolderSize(Path path) throws IOException {
        if(Files.isDirectory(path)) {
            return Files.walk(path)
                    .filter(p -> p.toFile().isFile())
                    .mapToLong(p -> p.toFile().length())
                    .sum()/KILOBYTE;
        } else {
            return path.toFile().length()/KILOBYTE;
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.type);
        builder.append(" ");
        builder.append(this.path.toString().toUpperCase());
        builder.append(" ");
        builder.append(this.size);
        builder.append("KB");
        return builder.toString();
    }

    @Override
    public int compareTo(DirItem o) {
        return (o.size).compareTo(this.size);
    }
}

enum Type {
    FILE,
    DIR
}


